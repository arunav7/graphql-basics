import { v4 as uuidv4 } from 'uuid'

const Mutation = {
    createUser: (parent, args, { db }, info) => {
        const isEmailTaken = db.users.some(user => user.email === args.user.email)
        if(isEmailTaken)
            throw new Error('Email Taken')

        const user = {
            id: uuidv4(),
            ...args.user
        }
        db.users.unshift(user)
        
        return user
    },
    deleteUser: (_, args, { db }) => {
        const index = db.users.findIndex(user => user.id === args.id)
        if( index === -1 )
            throw new Error('User not found') 
        
        const deletedUser = db.users.splice(index, 1)

        // deletes the post of the given user
        db.posts = db.posts.filter(post => {
            const match = post.author === args.id
            // deletes the comment whose id matches with the post
            if(match) {
                db.comments = db.comments.filter(comment => comment.postId !== post.id)
            }

            return !match
        })
        // deletes the comment of the given user
        db.comments = db.comments.filter(comment => comment.author !== args.id)

        return deletedUser[0]
    },
    updateUser: (_, args, { db }) => {
        const user = db.users.find(user => user.id === args.id)

        if(!user)
            throw new Error('User not found')

        if(typeof args.data.email === 'string') {
            const emailTaken = db.users.some(user => user.email === args.data.email)
            if(emailTaken)
                throw new Error('Email exists')

            user.email = args.data.email
        }

        if(typeof args.data.name === 'string')
            user.name = args.data.name

        if(typeof args.data.age !== 'undefined')
            user.age = args.data.age

        return user
    },
    createPost: (parent, args, { db, pubsub }, info) => {
        const userExists = db.users.some(user => user.id === args.post.author)
        if(!userExists)
            throw new Error('User not found')

        const post = {
            id: uuidv4(),
            ...args.post
        }
        db.posts.unshift(post)
        if(args.post.published)
            pubsub.publish('post', { 
                post: {
                    mutation: "CREATED",
                    data: post
                } 
            })

        return post
    },
    deletePost: (_, args, { db, pubsub }) => {
        const index = db.posts.findIndex(post => post.id === args.id)
        if(index === -1)
            throw new Error('No posts found')

        const [post] = db.posts.splice(index, 1)
        db.comments = db.comments.filter(comment => !(comment.postId === args.id))
        // db.comments = db.comments.filter(comment => comment.postId !== args.id)
        if(post.published)
            pubsub.publish('post', {
                post: {
                    mutation: 'DELETED',
                    data: post
                }
            })

        return post
    },
    updatePost: (_, args, { db, pubsub }) => {
        const { id, data } = args

        const post = db.posts.find(post => post.id === id)
        const originalPost = { ...post }
        if(!post)
            throw new Error('No Post found')
        
        if(!data)
            return post

        const updates = Object.keys(data)
        updates.forEach(update => {
            post[update] = data[update]

            if(originalPost.published && !post['published']) {
                pubsub.publish('post', {
                    post: {
                        mutation: 'DELETED',
                        data: originalPost
                    }
                })
            } else if(!originalPost.published && post['published']) {
                pubsub.publish('post', {
                    post: {
                        mutation: 'CREATED',
                        data: post
                    }
                })
            } else if(post['published'])
                pubsub.publish('post', {
                    post: {
                        mutation: 'UPDATED',
                        data: post
                    }
                })
        })

        return post
    },
    createComment: (parent, args, { db, pubsub }, info) => {
        const userExists = db.users.some(user => user.id === args.comment.author)
        if(!userExists)
            throw new Error('User not Found')
        
        const postExists = db.posts.some(post => post.id === args.comment.postId && post.published)
        if(!postExists)
            throw new Error('No posts for the user')
        
        const comment = {
            id: uuidv4(),
            ...args.comment
        }
        db.comments.unshift(comment)
        pubsub.publish(`comment ${args.comment.postId}`, { 
            comment: {
                mutation: 'CREATED',
                data: comment
            }  
        })

        return comment
    },
    deleteComment: (_, args, { db, pubsub }) => {
        const index = db.comments.findIndex(comment => comment.id === args.id)
        if(index === -1)
            throw new Error('No comment found')

        const [deletedComment] = db.comments.splice(index, 1)
        pubsub.publish(`comment ${deletedComment.postId}`, { 
            comment: {
                mutation: 'DELETED',
                data: deletedComment
            }  
        })

        return deletedComment
    },
    updateComment: (_, { id, data }, { db }, info) => {
        const comment = db.comments.find(comment => comment.id === id)
        if(!comment)
            throw new Error('No Comment found')

        if(typeof data.text === 'string')
            comment.text = data.text
        
        pubsub.publish(`comment ${comment.postId}`, {
            comment: {
                mutation: 'UPDATED',
                data: comment
            }
        })

        return comment
    }
}

export default Mutation